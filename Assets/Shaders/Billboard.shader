﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


 Shader "Cg  shader for billboards" {
    Properties {
       _MainTex ("Texture Image", 2D) = "white" {}
	   _Color ("Tint", Color) = (1,1,1,1)
       _CutOff("Cut off", float) = 0.1
    }
    SubShader {
       Pass {   
          CGPROGRAM
  
          #pragma vertex vert  
          #pragma fragment frag 
  
          // User-specified uniforms            
          uniform sampler2D _MainTex;        
          uniform float _CutOff;
		  uniform float4 _Color;

          struct vertexInput {
             float4 vertex : POSITION;
             float4 tex : TEXCOORD0;
          };

          struct vertexOutput {
             float4 pos : SV_POSITION;
             float4 tex : TEXCOORD0;
          };
  
          vertexOutput vert(vertexInput input) 
          {
             vertexOutput output;
  
              float4 ori=mul(UNITY_MATRIX_MV,float4(0,0,0,1));
            float2 r1=float2(unity_ObjectToWorld[0][0],unity_ObjectToWorld[0][2]);
            float2 r2=float2(unity_ObjectToWorld[2][0],unity_ObjectToWorld[2][2]);
			float4 vt=input.vertex;
			float2 vt0=vt.x*r1;
            vt0+=vt.z*r2;
            vt.xy=vt0;
            vt.z=0;
            vt.xyz+=ori.xyz;

             output.pos = mul(UNITY_MATRIX_P, vt);
              // - float4(input.vertex.x, input.vertex.z, 0.0, 0.0));
  
             output.tex = input.tex;
  
             return output;
           }
  
          float4 frag(vertexOutput input) : COLOR
          {
             
             float4 color = tex2D(_MainTex, float2(input.tex.xy)) * _Color;   
             if(color.a < _CutOff) discard;
             return color;
          }
  
          ENDCG
       }
    }
 }
 


 /*

Shader "Tut/Project/Billboard_2" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_CutOff("Cut off", float) = 0.1
    }
    SubShader {
        //Tags { "Queue"="AlphaTest" "IgnoreProjector"="false" "RenderType"="TransparentCutout" }
        pass{
        Cull Off
        ZTest Always
        Blend SrcAlpha OneMinusSrcAlpha
        CGPROGRAM
		uniform float _CutOff;
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"
        sampler2D _MainTex;
		fixed4 _Color;
        struct v2f {
            float4 pos:SV_POSITION;
            float2 texc:TEXCOORD0;
        };
        v2f vert(appdata_base v)
        {

            v2f o;
            float4 ori=mul(UNITY_MATRIX_MV,float4(0,0,0,1));
            float4 vt=v.vertex;
			float _CutOff;
            //vt.y=vt.z;
            float2 r1=float2(unity_ObjectToWorld[0][0],unity_ObjectToWorld[0][2]);
            float2 r2=float2(unity_ObjectToWorld[2][0],unity_ObjectToWorld[2][2]);
            float2 vt0=vt.x*r1;
            vt0+=vt.z*r2;
            vt.xy=vt0;
            vt.z=0;
            vt.xyz+=ori.xyz;//result is vt.z==ori.z ,so the distance to camera keeped ,and screen size keeped
            o.pos=mul(UNITY_MATRIX_P,vt);
 
            o.texc=v.texcoord;
            return o;
        }
        float4 frag(v2f i):COLOR
        {
			float4 col = tex2D(_MainTex,i.texc) * _Color;
			if(col.a < _CutOff) discard;
            return col;
        }
        ENDCG
        }//endpass
    }
}

Shader "Custom/Sprite_Billboard" {
     Properties
     {
         _MainTex ("Sprite Texture", 2D) = "white" {}
         _Color ("Tint", Color) = (1,1,1,1)
         //_Time ("Time", Float) = 0
         [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
     }
 
     SubShader
     {
         Tags
         { 
             "Queue"="Transparent"
             "SortingLayer"="Resources_Sprites" 
             "IgnoreProjector"="True" 
             "RenderType"="Transparent" 
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
			 "DisableBatching" = "True"
         }
 
         Cull Off
         Lighting Off
         ZWrite Off
         Blend One OneMinusSrcAlpha
 
         Pass
         {
         CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma target 2.0
             #pragma multi_compile _ PIXELSNAP_ON
             #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
             #include "UnityCG.cginc"
 
             uniform Float _Time;
 
             struct appdata_t
             {
                 float4 vertex   : POSITION;
                 float4 color    : COLOR;
                 float2 texcoord : TEXCOORD0;
                 UNITY_VERTEX_INPUT_INSTANCE_ID
             };
 
             struct v2f
             {
                 float4 vertex   : SV_POSITION;
                 fixed4 color    : COLOR;
                 float2 texcoord  : TEXCOORD0;
                 UNITY_VERTEX_OUTPUT_STEREO
             };
             
             fixed4 _Color;
 
             v2f vert(appdata_t IN)
             {
                 v2f OUT;
                 UNITY_SETUP_INSTANCE_ID(IN);
                 UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
               // OUT.vertex = UnityObjectToClipPos(IN.vertex);
                 OUT.texcoord = IN.texcoord;
                 OUT.color = IN.color * _Color;
             //    #ifdef PIXELSNAP_ON
 
                 OUT.vertex = mul(UNITY_MATRIX_P, 
                  mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0))
                 - float4(IN.vertex.y, IN.vertex.x, 0.0, 0.0)
                  * float4(0.1, 0.1, 1.0, 1.0));
 
                 //OUT.vertex = UnityPixelSnap (OUT.vertex);
             //    #endif
 
                 return OUT;
             }
 
             sampler2D _MainTex;
             sampler2D _AlphaTex;
 
             fixed4 SampleSpriteTexture (float2 uv)
             {
                 fixed4 color = tex2D (_MainTex, uv);
 
 #if ETC1_EXTERNAL_ALPHA
                 // get the color from an external texture (usecase: Alpha support for ETC1 on android)
                 color.a = tex2D (_AlphaTex, uv).r;
 #endif //ETC1_EXTERNAL_ALPHA
 
                 return color;
             }
 
             fixed4 frag(v2f IN) : SV_Target
             {
                 fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
                 c.rgb *= c.a;
                 return c;
             }
         ENDCG
         }
     }
 }

*/
 
