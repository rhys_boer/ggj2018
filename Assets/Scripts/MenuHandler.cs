﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuHandler : MonoBehaviour {

	public Button playButton;
	public Button muteButton;

	public GameObject credits;
	public GameObject menu;

	public Sprite muteSprite;
	public Sprite unmuteSprite;

	private Image muteImage;

	public GameObject canvasMenu;
	public AudioManager audioManager;
	public AlienSpawner spawner;

	private bool isMuted = false;

	private void Start() {
		muteImage = muteButton.GetComponent<Image>();
	}

	private void Update() {
		if (GameManager.GameState == GAME_STATE.MENU && Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
	}

	// Activate play button
	public void ActivateMainMenu() {
		playButton.gameObject.SetActive(true);
	}

	public void OnPlayClick() {
		GameManager.StartGame();
		spawner.Restart();

		playButton.gameObject.SetActive(false);
	}

	public void OnMuteClick() {
		if (isMuted == false) {
			audioManager.MuteAudio();
			isMuted = true;
			muteImage.sprite = unmuteSprite;
		} else {
			audioManager.UnmuteAudio();
			isMuted = false;
			muteImage.sprite = muteSprite;
		}
	}

	public void ToggleMenu() {
		if (canvasMenu.activeSelf) { 
			canvasMenu.SetActive(false);
			GameManager.Unpause();
		} else {
			canvasMenu.SetActive(true);
			GameManager.Pause();
		}
	}

	public void TurnOffMenu() {
		canvasMenu.SetActive(false);
		GameManager.Unpause();
	}

	public void QuitGame() {
		Application.Quit();
	}

	public void ToggleCredits() {
		if (credits.activeSelf) {
			credits.SetActive(false);
			menu.SetActive(true);
		}
		else {
			credits.SetActive(true);
			menu.SetActive(false);
		}
	}
}
