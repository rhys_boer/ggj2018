﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadioStatic : MonoBehaviour {

	public float speed;
	public float fadespeed;
	public List<Sprite> sprites = new List<Sprite>();

	private float currTime = 0.0f;

	private Image image;
	private int index = 0;

	private const float s_out = 0.7f;
	private const float s_weak = 0.6f;
	private const float s_mild = 0.4f;
	private const float s_strong = 0.2f;
	private const float s_point = 0.0f;

	private void Awake() {
		image = GetComponent<Image>();
	}

	private void Update() {
		if(GameManager.GameState == GAME_STATE.MENU) {
			if (currTime >= speed) {
				currTime = 0.0f;

				index++;
				if (index > sprites.Count - 1) {
					index = 0;
				}
			} else {
				currTime += Time.deltaTime;
			}

			image.sprite = sprites[index];
		}
	}

	public void SetStatic(TUNE_STRENGTH strengh) {
		if (image == null) return;
		Color color = image.color;

		switch (strengh) {
			case TUNE_STRENGTH.OUT_OF_SIGHT:
				color.a = Mathf.Lerp(color.a, s_out, Time.deltaTime * fadespeed); break;
			case TUNE_STRENGTH.WEAK:
				color.a = Mathf.Lerp(color.a, s_weak, Time.deltaTime * fadespeed); break;
			case TUNE_STRENGTH.MILD:
				color.a = Mathf.Lerp(color.a, s_mild, Time.deltaTime * fadespeed); break;
			case TUNE_STRENGTH.STRONG:
				color.a = Mathf.Lerp(color.a, s_strong, Time.deltaTime * fadespeed); break;
			case TUNE_STRENGTH.ON_POINT:
				color.a = Mathf.Lerp(color.a, s_point, Time.deltaTime * fadespeed); break;
		}

		if(currTime >= speed) {
			currTime = 0.0f;

			index++;
			if (index > sprites.Count - 1) {
				index = 0;
			}
		} else {
			currTime += Time.deltaTime;
		}

		image.sprite = sprites[index];
		image.color = color;
	}
}
