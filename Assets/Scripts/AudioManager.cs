﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public List<AudioSource> audioSources = new List<AudioSource>();

	public void MuteAudio() {
		foreach(AudioSource a in audioSources) {
			a.mute = true;
		}
	}

	public void UnmuteAudio() {
		foreach (AudioSource a in audioSources) {
			a.mute = false;
		}
	}
}
