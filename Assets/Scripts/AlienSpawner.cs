﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienSpawner : MonoBehaviour {

	public GameObject landScape;

	public List<GameObject> aliens = new List<GameObject>();
	public List<GameObject> spawnPoints = new List<GameObject>();

	private List<GameObject> usedSpawns = new List<GameObject>();
	private List<GameObject> spawnedAliens = new List<GameObject>();

	void Start () {
		
	}

	public void SpawnAlien() {
		if (spawnPoints.Count == 0) return;

		int alienIndex = Random.Range(0, aliens.Count);
		int spawnIndex = Random.Range(0, spawnPoints.Count);

		GameObject alien = Instantiate(aliens[alienIndex], spawnPoints[spawnIndex].transform.position, spawnPoints[spawnIndex].transform.rotation);
		alien.transform.parent = landScape.transform;

		spawnedAliens.Add(alien);

		usedSpawns.Add(spawnPoints[spawnIndex]);
		spawnPoints.RemoveAt(spawnIndex);
	}

	public void Restart() {
		if (usedSpawns.Count == 0) return;

		foreach(var sp in usedSpawns) {
			spawnPoints.Add(sp);
		}

		usedSpawns.Clear();

		foreach(var a in spawnedAliens) {
			Destroy(a);
		}

		spawnedAliens.Clear();
	}
}
