﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour {

	public float speed;

	public List<Texture> sprites = new List<Texture>();

	private int index = 0;
	private float currTime = 0.0f;

	private MeshRenderer render;

	void Start() {
		render = GetComponent<MeshRenderer>();
	}

	void Update () {
		this.transform.rotation = Quaternion.Euler(0,180,0);
		if (render == null) return;

		if(sprites.Count > 0) {
			if(currTime >= speed) {
				index++;

				if (index > sprites.Count - 1) index = 0;
				currTime = 0.0f;
			} else {
				currTime += Time.deltaTime;
			}
		}

		render.material.mainTexture = sprites[index];
	}
}
