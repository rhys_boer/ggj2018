﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GAME_STATE {
	PLAYING,
	START,
	MENU,
	PAUSE,
	WIN,
	LOSE,
	ERROR
}

public class GameManager : MonoBehaviour {

	private static GAME_STATE gamestate = GAME_STATE.MENU;
	public static GAME_STATE GameState { get { return gamestate; } }

	private MenuHandler menuHandler;

	private void Start() {
		GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");

		if(canvas != null) {
			menuHandler = canvas.GetComponent<MenuHandler>();
		} else {
			Debug.LogWarning("Couldn't find canvas! Make sure it has the tag Canvas!");
			gamestate = GAME_STATE.ERROR;
		}
	}

	private void Update() {
		switch (gamestate) {
			case GAME_STATE.PLAYING:
				break;
			case GAME_STATE.START:
				GameObject.FindGameObjectWithTag("Tuner").GetComponent<RadioTuner>().StartTuner();
				gamestate = GAME_STATE.PLAYING;
				break;
			case GAME_STATE.MENU:
				break;
			case GAME_STATE.WIN:
				menuHandler.ActivateMainMenu();
				gamestate = GAME_STATE.MENU;
				break;
			case GAME_STATE.LOSE:
				menuHandler.ActivateMainMenu();
				gamestate = GAME_STATE.MENU;
				break;
			case GAME_STATE.ERROR:
				break;
			default: break;
		}
	}

	public static void Pause() {
		gamestate = GAME_STATE.PAUSE;
	}

	public static void Unpause() {
		gamestate = GAME_STATE.PLAYING;
	}

	public static void SetError(string errorMessage = "unknown") {
		Debug.LogError("Error Message: " + errorMessage);
		gamestate = GAME_STATE.ERROR;
	}

	public static void StartGame() {
		gamestate = GAME_STATE.START;
	}

	public static void Lose() {
		gamestate = GAME_STATE.LOSE;
	}

	public static void Win() {
		gamestate = GAME_STATE.WIN;
	}
}
