﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TUNE_STRENGTH {
	// The higher the better
	OUT_OF_SIGHT,	// 0 - Very far away
	WEAK,			// 1 - Close
	MILD,			// 2 - Getting closer
	STRONG,			// 3 - Strong
	ON_POINT,		// 4 - On Point
}

public class RadioTuner : MonoBehaviour {

	public RadioStatic radio;
	public AlienSpawner spawner;

	public bool debug = false;

    private const int maxKhz = 1710;
    private const int minKhz = 522;
	private const float khzAmount = 1.25f;

	private const float strength_out = 300.0f;
	private const float strength_weak = 250.0f;
	private const float strength_mild = 100.0f;
	private const float strength_strong = 25.0f;
	private const float strength_point = 2.5f;

	private float targetKhz;

	private float curKhz = (maxKhz + minKhz) / 2;

	private const float sendTime = 2.5f; // Total time to send a message
	private const float gameTime = 80.0f; // Total time in the game
	private float currSendTime = 0.0f;
	private float currGameTime = 0.0f; // Game Timer

	private bool[] sentSignals = { false, false, false };
	private int totalSentSignals = 0;

	private TUNE_STRENGTH signalStrength = TUNE_STRENGTH.OUT_OF_SIGHT;

	public RectTransform radioBar;
	private int totalRadioBars = 0;
	private float totalWidth = 0.0f;
	private float notchOnBar = 0.0f;

	// Use this for initialization
	void Start () {
		totalRadioBars = radioBar.childCount;
		totalWidth = totalRadioBars * radioBar.GetChild(0).GetComponent<RectTransform>().rect.width;
		notchOnBar = totalRadioBars / 2;
	}

	// Update is called once per frame
	void Update() {
		if (GameManager.GameState != GAME_STATE.PLAYING) return;

		Controls();
		GetTuneStrength();

		if(signalStrength == TUNE_STRENGTH.ON_POINT) {
			if(currSendTime > sendTime) {
				sentSignals[totalSentSignals] = true;
				totalSentSignals++;

				spawner.SpawnAlien();

				GenerateNewKhzTarget();

			} else {
				currSendTime += Time.deltaTime;
			}
		} else {
			currSendTime = 0.0f;
		}

		if (totalSentSignals >= sentSignals.Length) {
			// WIN HERE!
			GameManager.Win();
			return;
		}

		Crossfade.CrossfadeAudio(signalStrength);
		MoveRadioBar();

		if(radio.gameObject.activeSelf)
			radio.SetStatic(signalStrength);

		currGameTime -= Time.deltaTime;



		if(currGameTime <= 0.0f) {
			GameManager.Lose();
		}
	}

	public void StartTuner() {
		GenerateNewKhzTarget();
		currGameTime = gameTime;
		sentSignals = new bool[3] { false, false, false };
		totalSentSignals = 0;
		curKhz = (maxKhz + minKhz) / 2;
	}

	private void Controls() {
		// Tune Up
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
			curKhz += khzAmount;
		}

		// Tune Down
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
			curKhz -= khzAmount;
		}

		// Escape
		if (Input.GetKey(KeyCode.Escape)) {
			GameManager.Lose();
		}

		// DESTORY ON RELEASE
		if (Input.GetKeyDown(KeyCode.P)) {
			if (debug == false) return;
			spawner.SpawnAlien();
		}

		if (curKhz > maxKhz) {
			curKhz = maxKhz;
		}
		else if (curKhz < minKhz) {
			curKhz = minKhz;
		}
	}

	private void GetTuneStrength() {
		// OUT OF SIGHT
		if(curKhz < targetKhz - strength_out || curKhz > targetKhz + strength_out) {
			signalStrength = TUNE_STRENGTH.OUT_OF_SIGHT;
		}

		// WEAK
		if(curKhz > targetKhz - strength_weak && curKhz < targetKhz + strength_weak) {
			signalStrength = TUNE_STRENGTH.WEAK;
		}

		// MILD
		if (curKhz > targetKhz - strength_mild && curKhz < targetKhz + strength_mild) {
			signalStrength = TUNE_STRENGTH.MILD;
		}

		// STRONG
		if (curKhz > targetKhz - strength_strong && curKhz < targetKhz + strength_strong) {
			signalStrength = TUNE_STRENGTH.STRONG;
		}

		// ON POINT
		if (curKhz > targetKhz - strength_point && curKhz < targetKhz + strength_point) {
			signalStrength = TUNE_STRENGTH.ON_POINT;
		}
	}

	private void GenerateNewKhzTarget() {
		targetKhz = Random.Range(minKhz, maxKhz);
	}

	private float KhzToRadioPoint() {
		return totalWidth * (curKhz - minKhz) / (maxKhz - minKhz);
	}

	private void MoveRadioBar() {
		// (Random.Range(0, jitterMax) - (jitterMax / 2))) * jitterMultiplier
		// float radioPoint = -KhzToRadioPoint() + 350.0f;
		// float jitter = Mathf.Lerp(radioPoint, radioPoint + (Random.Range(0, jitterMax) - (jitterMax / 2)) * jitterMultiplier, 0.3f);
		// radioBar.offsetMin = new Vector2(jitter, 0);

		radioBar.offsetMin = new Vector2((-KhzToRadioPoint() + 350.0f), 0);
	}

	// Debug
	private void OnGUI() {
		if (debug == false) return;
		GUI.Label(new Rect(10, 10, 200, 25), "Debug Info");
		GUI.Label(new Rect(10, 25, 200, 25), "Current Khz: " + curKhz.ToString() + "	Khz");
		GUI.Label(new Rect(10, 40, 200, 25), "Target Khz: " + targetKhz.ToString() + "	Khz");
		GUI.Label(new Rect(10, 55, 200, 25), "Tune Strengh: " + signalStrength.ToString());
		GUI.Label(new Rect(10, 70, 200, 25), "Sending Time: " + currSendTime + "/" + sendTime);
		GUI.Label(new Rect(10, 85, 200, 25), "Sent Signals: " +
			"[" + ((sentSignals[0] == true) ? "X" : " ") + "] " +
			"[" + ((sentSignals[1] == true) ? "X" : " ") + "] " +
			"[" + ((sentSignals[2] == true) ? "X" : " ") + "]" );
		GUI.Label(new Rect(10, 100, 200, 25), "Timer: " + currGameTime);
		GUI.Label(new Rect(10, 115, 200, 25), "Game State: " + GameManager.GameState);
		GUI.Label(new Rect(10, 130, 250, 25), "Static Strength: " + Crossfade.VolumeStatic);
		GUI.Label(new Rect(10, 145, 250, 25), "Transmission Strength: " + Crossfade.VolumeTransmission);
		GUI.Label(new Rect(10, 160, 200, 25), "Bar Position: " + KhzToRadioPoint());
	}
}