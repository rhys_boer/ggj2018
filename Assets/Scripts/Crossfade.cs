﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossfade : MonoBehaviour {

	private AudioSource audio1;
	private AudioSource audio2;

	private static float volume1 = 1.0f;
	private static float volume2 = 0.0f;

	private const float signalFadeStrength = 0.55f;
	private const float signal_out = 0.0f;
	private const float signal_weak = 0.2f;
	private const float signal_mild = 0.4f;
	private const float signal_strong = 0.6f;
	private const float signal_point = 1.0f;

	public static float VolumeStatic { get { return volume1; } }
	public static float VolumeTransmission { get { return volume2; } }

	void Start () {
		GameObject objStatic = this.transform.GetChild(0).gameObject;
		GameObject objStation = this.transform.GetChild(1).gameObject;

		if(objStatic == null || objStation == null) {
			GameManager.SetError("Could not find children in Radio Tuner.");
		} else {
			audio1 = objStatic.GetComponent<AudioSource>();
			audio2 = objStation.GetComponent<AudioSource>();

			audio1.volume = volume1;
			audio2.volume = volume2;
		}
	}
	
	void Update () {
		audio1.volume = volume1;
		audio2.volume = volume2;
	}

	public static void CrossfadeAudio(TUNE_STRENGTH signal) {
		switch (signal) {
			case TUNE_STRENGTH.OUT_OF_SIGHT:
				volume1 = Mathf.Lerp(volume1, signal_point, signalFadeStrength);
				volume2 = Mathf.Lerp(volume2, signal_out, signalFadeStrength);
				break;
			case TUNE_STRENGTH.WEAK:
				volume1 = Mathf.Lerp(volume1, signal_strong, signalFadeStrength);
				volume2 = Mathf.Lerp(volume2, signal_weak, signalFadeStrength);
				break;
			case TUNE_STRENGTH.MILD:
				volume1 = Mathf.Lerp(volume1, signal_mild, signalFadeStrength);
				volume2 = Mathf.Lerp(volume2, signal_mild, signalFadeStrength);
				break;
			case TUNE_STRENGTH.STRONG:
				volume1 = Mathf.Lerp(volume1, signal_weak, signalFadeStrength);
				volume2 = Mathf.Lerp(volume2, signal_strong, signalFadeStrength);
				break;
			case TUNE_STRENGTH.ON_POINT:
				volume1 = Mathf.Lerp(volume1, signal_out, signalFadeStrength);
				volume2 = Mathf.Lerp(volume2, signal_point, signalFadeStrength);
				break;
			default: break;
		}
	}
}
