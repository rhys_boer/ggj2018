﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioBar : MonoBehaviour {


	public List<GameObject> objectAtivate = new List<GameObject>();
	public List<GameObject> deactivate = new List<GameObject>();

	// Update is called once per frame
	void Update() {
		if (objectAtivate.Count == 0) return;

		if (GameManager.GameState == GAME_STATE.MENU) {
			foreach(var o in objectAtivate) {
				o.SetActive(true);
			}

			foreach (var o in deactivate) {
				o.SetActive(false);
			}
		} else {
			foreach (var o in objectAtivate) {
				o.SetActive(false);
			}

			foreach (var o in deactivate) {
				o.SetActive(true);
			}
		}
	}
}
