﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : MonoBehaviour {

	public float inTime = 2.0f;
	private float currTime = 0.0f;

	private SkinnedMeshRenderer render;

	void Start () {
		render = GetComponent<SkinnedMeshRenderer>();
		render.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(currTime > inTime) {
			render.enabled = true;
		} else {
			currTime += Time.deltaTime;
		}
	}
}
